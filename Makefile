RM	= rm -f

CPP	= g++ -lpthread -lSDL -lSDL_gfx

CPPFLAGS	+= -Wall -Wextra -std=c++11 -g

NAME	= plazza

SRC	= src/main.cpp			\
	  src/Pizza.cpp			\
	  src/Reception.cpp		\
	  src/Kitchen.cpp		\
	  src/Select.cpp		\
	  src/utils/TimeGiver.cpp	\
	  src/utils/Conversion.cpp	\
	  src/utils/Errors.cpp		\
	  src/utils/Logger.cpp		\
	  src/process/Process.cpp	\
	  src/process/PThread.cpp	\
	  src/mutex/Mutex.cpp		\
	  src/condvar/Condvar.cpp	\
	  src/ParsePizza.cpp		\
	  src/process/ThreadPool.cpp	\
	  src/Cook.cpp			\
	  src/UI/SDL.cpp

OBJ	= $(SRC:.cpp=.o)


all: $(NAME)

$(NAME): $(OBJ)
	$(CPP) -o $(NAME) $(OBJ)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
