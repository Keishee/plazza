/* 
 * File:   Reception.hpp
 * Author: Clement Da Silva
 *
 * Created on April 20, 2015, 8:32 PM
 */

#ifndef RECEPTION_HPP_
#define RECEPTION_HPP_

#include <map>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <vector>
#include <cstring>
#include <stdexcept>
#include <iostream>
#include <cstdlib>
#include <sstream>
#include <unistd.h>
#include <algorithm>
#include "process/Process.hpp"
#include "utils/Errors.hpp"
#include "Pizza.hpp"
#include "Select.hpp"
#include "Kitchen.hpp"
#include "utils/Conversion.hpp"
#include "utils/Logger.hpp"

class Reception {
private:
    float _multiplier;
    int _cooks;
    int _ingTime;
    Select *select;
    std::vector<int> fifoInVector;
    std::vector<int> fifoOutVector;
    std::vector<int> totalFDs;
    std::map<std::string, TypePizza> typePizzaMap;
    std::map<std::string, TaillePizza> taillePizzaMap;
    std::map<int, int> fdKitchen;
    int _kitchenCount;

    void sendOrder(Pizza* pizza);
    int createKitchen();
    void getAndSendPizza(const std::string& serialPizza);
    void parseResult(const std::string&);
    std::vector<Pizza *>* registerOrder(const std::string& order);
public:
    Reception(char* multiplier, char* cooks, char *ingTime);
    Reception(const Reception& orig);
    virtual ~Reception();

    void begin();
};

#endif	/* !RECEPTION_HPP_ */

