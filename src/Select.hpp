/* 
 * File:   Select.hpp
 * Author: dana_a
 *
 * Created on April 21, 2015, 5:45 PM
 */

#ifndef SELECT_HPP
#define	SELECT_HPP

#include <sys/select.h>
#include <vector>
#include <stdexcept>
#include <unistd.h>
#include <strings.h>
#include "utils/Errors.hpp"

class Select {
    int maxFd;
    int size_max;
    fd_set setRead;
    fd_set setWrite;
    std::vector<int> fds;

public:
    Select(const std::vector<int>&);
    virtual ~Select();

    void addFd(int fd);
    void removeFD(int fd);
    int launchSelect(std::string& buffer);
};

#endif	/* SELECT_HPP */

