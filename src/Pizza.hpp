/* 
 * File:   Pizza.hpp
 * Author: Clement Da Silva
 *
 * Created on April 21, 2015, 5:02 PM
 */

#ifndef PIZZA_HPP_
#define PIZZA_HPP_

#include <sstream>
#include <iostream>
#include <vector>
#include <map>
#include "Pizza.hpp"
#include "mutex/Mutex.hpp"
#include "condvar/Condvar.hpp"

extern float g_multiplier;
extern Mutex mutex;
extern std::vector<Condvar *> condVars;

enum TypePizza {
    Regina = 1,
    Margarita = 2,
    Americaine = 4,
    Fantasia = 8
};

enum TaillePizza {
    S = 1,
    M = 2,
    L = 4,
    XL = 8,
    XXL = 16
};

class Pizza {
    bool _made;
    bool _packed;
    TaillePizza _size;
    TypePizza _type;
    std::map<TypePizza, std::string> pizzaTypeMap;
    std::map<TaillePizza, std::string> pizzaTailleMap;
public:
    Pizza(TaillePizza size, TypePizza type);
    Pizza(const Pizza& orig);
    virtual ~Pizza();

    bool isMade() const;
    bool isPacked() const;
    TaillePizza getTaillePizza() const;
    TypePizza getTypePizza() const;
    std::string getRealType() ;
    std::string getRealSize() ;

    void setMade(bool);
    void setPacked(bool);
    const char* pack();
    void unpack();
    void setTaillePizza(TaillePizza);
    void setTypePizza(TypePizza);

    Pizza& operator=(const Pizza& right);

};

#endif	/* !PIZZA_HPP_ */

