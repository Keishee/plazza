/* 
 * File:   Kitchen.cpp
 * Author: dana_a
 * 
 * Created on April 21, 2015, 4:30 PM
 */

#include <cstdlib>
#include <vector>
#include <string.h>
#include <strings.h>
#include "Kitchen.hpp"
#include "Select.hpp"
#include "process/ThreadPool.hpp"
#include "ParsePizza.hpp"
#include "Cook.hpp"
#include "utils/TimeGiver.hpp"
#include "utils/Conversion.hpp"
#include "UI/SDL.h"

Kitchen::Kitchen(const std::string& pipe_in,
        const std::string& pipe_out,
        const int cooks, const int kitchen,
        const int regeneration) {
    this->cooks = cooks;
    this->kitchen = kitchen;
    this->regeneration = regeneration;
    pipin = open(pipe_in.c_str(), O_NONBLOCK | O_RDWR);
    pipout = open(pipe_out.c_str(), O_NONBLOCK | O_RDWR);
}

Kitchen::~Kitchen() {
    std::string retFd;
    retFd.append("-1 ")
            .append(Conversion<int>::toString(this->kitchen))
            .append(" ")
            .append(Conversion<int>::toString(this->pipout))
            .append(" ")
            .append(Conversion<int>::toString(this->pipin));
    write(this->pipout, retFd.c_str(), strlen(retFd.c_str()));
    close(this->pipin);
    close(this->pipout);
}

int Kitchen::action() {
    std::vector<int> fds;
    fds.push_back(this->pipin);
    Select select(fds);
    Mutex *lockPizza = new Mutex();
    ParsePizza *parse = new ParsePizza();
    unsigned long long timePassed = TimeGiver::timeMiS();
    SDL *window = new SDL(this->cooks, this->kitchen);
    ThreadPool pool(this->cooks, lockPizza, this->kitchen, window);
    Pizza *pizza;
    std::string request;

    pool.start();

    while (1) {
        window->drawCooks();
        request.clear();
        if (select.launchSelect(request) != -1) {
            if (request.compare("FULL") == 0) {
                if (pool.full() == false)
                    write(this->pipout, "GOOD", 4);
                else if (pool.full() == true)
                    write(this->pipout, "NOP", 3);
            } else if (request.compare("FULL") != 0) {
                pizza = parse->getPizza(request);
                pool.sendPizza(pizza);
            }
            request.clear();
        }
        lockPizza->lock();
        if (pool.getCookedPizzas().empty() == false) {
            timePassed = TimeGiver::timeMiS();
            pizza = pool.getCookedPizzas()[0];
            std::vector<Pizza*>::iterator it = pool.getCookedPizzas().begin();
            pool.getCookedPizzas().erase(it);
            write(this->pipout, pizza->pack(), strlen(pizza->pack()));
        }
        if (pool.idle() == false) {
            timePassed = TimeGiver::timeMiS();
        } else {
            if (TimeGiver::timeMiS() - 5000000 > timePassed) {
                delete lockPizza;
                delete parse;
                return (0);
            }
        }
        lockPizza->unlock();
        usleep(100);
    }
    return 1;
}
