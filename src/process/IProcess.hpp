/* 
 * File:   IProcess.hpp
 * Author: Clement Da Silva
 *
 * Created on April 20, 2015, 8:01 PM
 */

#ifndef IPROCESS_HPP_
#define IPROCESS_HPP_

class IProcess {
public:
    virtual ~IProcess();
    virtual int launch() = 0;
    virtual int getReturnCode() const = 0;
};

#endif	/* IPROCESS_HPP_ */

