/* 
 * File:   Process.hpp
 * Author: Clement Da Silva
 *
 * Created on April 20, 2015, 8:03 PM
 */

#ifndef PROCESS_HPP_
#define PROCESS_HPP_

#include <unistd.h>
#include <iostream>
#include "Processable.hpp"
#include "IProcess.hpp"

class Process : public IProcess {
private:
    Processable* _object;
    int returnCode;

public:
    Process(Processable* object);
    virtual ~Process();

    virtual int getReturnCode() const;
    virtual int launch();
};

#endif	/* !PROCESS_HPP_ */

