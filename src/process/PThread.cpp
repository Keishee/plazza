/* 
 * File:   PThread.cpp
 * Author: dana_a
 * 
 * Created on April 21, 2015, 10:58 AM
 */

#include <cstdlib>
#include <cstddef>
#include <stdexcept>
#include "PThread.hpp"
#include "IPThread.hpp"

PThread::PThread() {
    this->status = NO_USE;
    this->thread = new pthread_t;
}

IPThread::~IPThread() {
}

PThread::~PThread() {
    this->killThread();
    pthread_join(*this->thread, 0);
}

void PThread::joinThread() {
    if (pthread_join(*this->thread, 0) != 0)
        throw PTMCError("Thread Join Error");
}

void PThread::killThread() {
    if (pthread_cancel(*this->thread) != 0)
        throw PTMCError("Thread Cancel Error");
    this->status = DEAD;
}

void PThread::launch_thread(threadFunction func, void *params) {
    this->status = RUNNING;
    this->params = params;
    if (pthread_create(this->thread, 0, func, params) != 0) {
        throw PTMCError("Thread Create Error");
    }
}

t_status PThread::getStatus() {
    return (this->status);
}

void *PThread::getParams() {
    return (this->params);
}

void PThread::SetParams(void* params) {
    this->params = params;
}

pthread_t *PThread::getHandle() {
    return (this->thread);
}

