/* 
 * File:   ThreadPool.hpp
 * Author: barbis_j
 *
 * Created on April 21, 2015, 5:46 PM
 */

#ifndef THREADPOOL_HPP
#define	THREADPOOL_HPP

#include <iostream>
#include <vector>
#include <unistd.h>
#include "../Cook.hpp"
#include "PThread.hpp"
#include "../Pizza.hpp"
#include "../mutex/Mutex.hpp"
#include "../UI/SDL.h"

class ThreadPool {
public:
    ThreadPool(unsigned int num, Mutex *_mutex2, int _kitchenId, SDL *sdl);
    ThreadPool(const ThreadPool& orig);
    virtual ~ThreadPool();
    ThreadPool& operator=(const ThreadPool& orig);
    
    void start();
    bool full();
    void sendPizza(Pizza *pizza);
    
    std::vector<Pizza*>& getPizzas();
    unsigned int getThreadNum() const;
    const std::vector<Cook*>& getCooks() const;
    std::vector<Pizza *>& getCookedPizzas();
    std::vector<Pizza *>& getCookingPizzas();
    bool idle();

private:
    unsigned int threadNum;
    PThread thread;
    Mutex *lockPizzas;
    std::vector<Pizza *> pizzas;
    std::vector<Cook *> cooks;
    std::vector<Pizza *> cookedPizzas;
    std::vector<Pizza *> cookingPizzas;
    int kitchenId;
};

void *allocatePizza(void *arg);

#endif	/* THREADPOOL_HPP */

