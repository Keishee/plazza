/* 
 * File:   Process.cpp
 * Author: Clement Da Silva
 * 
 * Created on April 20, 2015, 8:03 PM
 */

#include "Process.hpp"

Process::Process(Processable* object) {
    this->_object = object;
}

Process::~Process() {
}

Processable::~Processable() {
}

IProcess::~IProcess() {
}

// GETTERS & SETTERS ===========================================================

int Process::getReturnCode() const {
    return this->returnCode;
}

// FUNCTIONS ===================================================================

int Process::launch() {
    this->returnCode = 0;
    if (fork() == 0) {
        try {
            this->returnCode = _object->action();
        } catch (std::exception& e) {
            std::cerr << e.what() << std::endl;
        }
        if (_object)
            delete _object;
        exit(1);
    }
    return 1;
}