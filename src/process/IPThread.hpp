/* 
 * File:   IPThread.hpp
 * Author: dana_a
 *
 * Created on April 21, 2015, 10:58 AM
 */

#ifndef IPTHREAD_HPP
#define	IPTHREAD_HPP

#include <pthread.h>

typedef void * (*threadFunction)(void *);

typedef enum e_status {
    NO_USE,
    RUNNING,
    DEAD,
} t_status;

class IPThread {
public:
    virtual ~IPThread();
    virtual void joinThread() = 0;
    virtual void *getParams() = 0;
    virtual void launch_thread(threadFunction, void *) = 0;
    virtual void SetParams(void *params) = 0;
    virtual void killThread() = 0;
    virtual pthread_t *getHandle() = 0;
    virtual t_status getStatus() = 0;
};

#endif	/* IPTHREAD_HPP */

