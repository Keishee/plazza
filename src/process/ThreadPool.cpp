/* 
 * File:   ThreadPool.cpp
 * Author: barbis_j
 * 
 * Created on April 21, 2015, 5:46 PM
 */

#include "ThreadPool.hpp"
#include "../condvar/Condvar.hpp"

ThreadPool::ThreadPool(unsigned int num, Mutex *_mutex2, int _kitchenId, SDL *sdl) {
    lockPizzas = _mutex2;
    threadNum = num;
    unsigned int i;
    kitchenId = _kitchenId;
    for (i = 0; i < threadNum; ++i) {
        condVars.push_back(new Condvar);
    }
    for (i = 0; i < threadNum; ++i) {
        cooks.push_back(new Cook(i, &cookedPizzas, lockPizzas, kitchenId, sdl));
    }
}

ThreadPool::ThreadPool(const ThreadPool& orig) {
    cooks = orig.cooks;
}

ThreadPool::~ThreadPool() {
    unsigned int i;
    for (i = 0; i < threadNum; ++i) {
        delete condVars.at(i);
    }    
    for (i = 0; i < threadNum; ++i) {
        delete cooks.at(i);
    }
}

ThreadPool& ThreadPool::operator=(const ThreadPool& orig) {
    if (this == &orig)
        return *this;
    return *this;
}

void ThreadPool::start() {
    thread.launch_thread(allocatePizza, this);
}

bool ThreadPool::full() {
    int count = 0;
    for (Cook *c : cooks)
    {
        if (c->getIsCooking())
            ++count;
    }
    if (threadNum * 2 == pizzas.size() + count)
        return true;
    return false;
}

void ThreadPool::sendPizza(Pizza* pizza) {
    pizzas.push_back(pizza);
}

void *allocatePizza(void *arg) {
    ThreadPool *pool = ((ThreadPool *) arg);
    while (1) {
        if (!pool->getPizzas().empty()) {
            for (unsigned int i = 0; i < pool->getThreadNum(); ++i) {
                if (!pool->getCooks().at(i)->getIsCooking()) {
                    pool->getCooks().at(i)->cookPizza(pool->getPizzas().at(0));
                    pool->getPizzas().erase(pool->getPizzas().begin());
                    condVars.at(i)->broadcast();
                    break;
                }
            }
        }
        usleep(50);
    }
    return NULL;
}

std::vector<Pizza*>& ThreadPool::getPizzas() {
    return pizzas;
}

unsigned int ThreadPool::getThreadNum() const {
    return threadNum;
}

const std::vector<Cook*>& ThreadPool::getCooks() const {
    return cooks;
}

std::vector<Pizza*>& ThreadPool::getCookedPizzas() {
    return cookedPizzas;
}

std::vector<Pizza*>& ThreadPool::getCookingPizzas() {
    return cookingPizzas;
}

bool ThreadPool::idle() {
    for (Cook *c : cooks) {
        if (c->getIsCooking())
            return false;
    }
    return true;
}
