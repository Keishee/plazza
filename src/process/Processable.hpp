/* 
 * File:   Object.hpp
 * Author: Clement Da Silva
 *
 * Created on April 20, 2015, 8:15 PM
 */

#ifndef PROCESSABLE_HPP_
#define PROCESSABLE_HPP_

class Processable {
public:
    virtual ~Processable();
    /**
     * This method will be called in the created process.
     * the process will close and destroy your object as soon as
     * it leaves this method
     * @return a return code
     */
    virtual int action() = 0;
};


#endif	/* OBJECT_HPP_ */

