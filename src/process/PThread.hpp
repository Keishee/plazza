/* 
 * File:   PThread.hpp
 * Author: dana_a
 *
 * Created on April 21, 2015, 10:58 AM
 */

#ifndef PTHREAD_HPP
#define	PTHREAD_HPP

#include "IPThread.hpp"
#include "../utils/Errors.hpp"

class PThread : public IPThread {
    pthread_t *thread;
    t_status status;
    void* params;

public:
    PThread();
    virtual ~PThread();

    void launch_thread(threadFunction, void *);
    void SetParams(void* params);

    void joinThread();
    void *getParams();

    pthread_t *getHandle();
    t_status getStatus();
    void killThread();

};

#endif	/* PTHREAD_HPP */

