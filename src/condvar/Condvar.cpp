/* 
 * File:   Condvar.cpp
 * Author: barbis_j
 * 
 * Created on April 21, 2015, 4:58 PM
 */

#include <stdexcept>

#include "Condvar.hpp"

Condvar::Condvar(/*IMutex *_mutex*/) {
    cond = PTHREAD_COND_INITIALIZER;
    //mutex = _mutex;
}

Condvar::Condvar(const Condvar& orig) {
    cond = orig.cond;
}

Condvar::~Condvar() {
}

ICondvar::~ICondvar() {
}

Condvar& Condvar::operator=(const Condvar& orig) {
    if (this == &orig)
        return *this;
    return *this;
}

void Condvar::broadcast() {
    if (pthread_cond_broadcast(&cond) != 0)
        throw PTMCError("Broadcast Error");
}

void Condvar::signal() {
    if (pthread_cond_signal(&cond) != 0)
        throw PTMCError("Signal Error");
}

void Condvar::wait() {
    mutex.lock();
    if (pthread_cond_wait(&cond, &mutex.getMutex()) != 0)
        throw PTMCError("CondWait Error");
    mutex.unlock();
}
