/* 
 * File:   Condvar.hpp
 * Author: barbis_j
 *
 * Created on April 21, 2015, 4:58 PM
 */

#ifndef CONDVAR_HPP
#define	CONDVAR_HPP

#include <pthread.h>
#include "../mutex/Mutex.hpp"
#include "../utils/Errors.hpp"
#include "ICondvar.hpp"

class Condvar : public ICondvar {
public:
    Condvar(/*IMutex *_mutex*/);
    Condvar(const Condvar& orig);
    virtual ~Condvar();
    Condvar& operator=(const Condvar& orig);

    virtual void broadcast();
    virtual void signal();
    virtual void wait();
private:
    pthread_cond_t cond;
    Mutex mutex;
    
};

#endif	/* CONDVAR_HPP */

