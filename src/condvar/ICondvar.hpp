/* 
 * File:   ICondvar.hpp
 * Author: barbis_j
 *
 * Created on April 21, 2015, 5:02 PM
 */

#ifndef ICONDVAR_HPP
#define	ICONDVAR_HPP

#include "../mutex/IMutex.hpp"

class ICondvar {
public:
    virtual ~ICondvar(void);
    virtual void wait(void) = 0;
    virtual void signal(void) = 0;
    virtual void broadcast(void) = 0;
};

#endif	/* ICONDVAR_HPP */

