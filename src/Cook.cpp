/* 
 * File:   Cook.cpp
 * Author: barbis_j
 * 
 * Created on April 22, 2015, 4:28 PM
 */

#include "utils/Logger.hpp"
#include "Cook.hpp"
#include "UI/SDL.h"

Cook::Cook(unsigned int _id, std::vector<Pizza *> *_cookedPizzas, Mutex *_lockPizza, int _kitchenId, SDL *sdl) {
    isCooking = false;
    id = _id;
    thread = new PThread;
    cookedPizzas = _cookedPizzas;
    thread->launch_thread(tmp, this);
    lockPizza = _lockPizza;
    cookingTime[Regina] = 1000000;
    cookingTime[Margarita] = 2000000;
    cookingTime[Americaine] = 2000000;
    cookingTime[Fantasia] = 4000000;
    kitchenId = _kitchenId;
    _sdl = sdl;
}

Cook::Cook(const Cook& orig) {
    isCooking = orig.isCooking;
}

Cook::~Cook() {
    //    delete thread;
}

Cook& Cook::operator=(const Cook& orig) {
    if (this == &orig)
        return *this;
    return *this;
}

void Cook::cookPizza(Pizza *pizza) {
    currentPizza = pizza;
}

void *tmp(void *arg) {
    Cook *cook = ((Cook *) arg);
    while (1) {
        condVars.at(cook->getId())->wait();
        cook->setIsCooking(true);
        std::cout << "Cook " << cook->getKitchenId() << ":" << cook->getId() << " has started cooking" << std::endl;
        (*Logger::getInstance()) << "Cook " << cook->getKitchenId() << ":" << cook->getId() << " has started cooking\n";
        cook->getSdl()->setState(cook->getId(), true);
        usleep(cook->getCookingTime()[cook->getCurrentPizza()->getTypePizza()] * g_multiplier);
        cook->getCurrentPizza()->setMade(true);
        std::cout << "Cook " << cook->getKitchenId() << ":" << cook->getId() << " has finished working" << std::endl;
        (*Logger::getInstance()) << "Cook " << cook->getKitchenId() << ":" << cook->getId() << " has finished working\n";
        cook->getLockPizza()->lock();
        cook->getCookedPizza()->push_back(cook->getCurrentPizza());
        cook->getLockPizza()->unlock();
        std::cout << "Cook " << cook->getKitchenId() << ":" << cook->getId() << " has sent the pizza to the kitchen" << std::endl;
        (*Logger::getInstance()) << "Cook " << cook->getKitchenId() << ":" << cook->getId() << " has sent the pizza to the kitchen\n";
        cook->getSdl()->setState(cook->getId(), false);
        cook->setIsCooking(false);
    }
    return NULL;
}

std::map<TypePizza, int> Cook::getCookingTime() const {
    return cookingTime;
}

void Cook::setCookingTime(std::map<TypePizza, int> cookingTime) {
    this->cookingTime = cookingTime;
}

Pizza *Cook::getCurrentPizza() {
    return currentPizza;
}

bool Cook::getIsCooking() const {
    return isCooking;
}

void Cook::setIsCooking(bool Cooking) {
    isCooking = Cooking;
}

PThread* Cook::getThread() const {
    return thread;
}

void Cook::setThread(PThread* thread) {
    this->thread = thread;
}

unsigned int Cook::getId() const {
    return id;
}

std::vector<Pizza*> *Cook::getCookedPizza() {
    return cookedPizzas;
}

Mutex* Cook::getLockPizza() {
    return lockPizza;
}

int Cook::getKitchenId() const {
    return kitchenId;
}

SDL* Cook::getSdl() {
    return _sdl;
}
