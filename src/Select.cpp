/* 
 * File:   Select.cpp
 * Author: dana_a
 * 
 * Created on April 21, 2015, 5:45 PM
 */

#include "Select.hpp"

Select::Select(const std::vector<int>& fds) {
    FD_ZERO(&this->setRead);
    FD_ZERO(&this->setWrite);
    this->size_max = fds.size();
    for (int i = 0; i < size_max; ++i) {
        this->fds.push_back(fds[i]);
        FD_SET(this->fds[i], &this->setRead);
        FD_SET(this->fds[i], &this->setWrite);
        this->maxFd = fds[i];
    }
}

Select::~Select() {
    FD_ZERO(&this->setRead);
    FD_ZERO(&this->setWrite);
}

void Select::addFd(int newFD) {
    FD_SET(newFD, &this->setRead);
    FD_SET(newFD, &this->setWrite);
    this->fds.push_back(newFD);
    for (int fd : fds) {
        if (fd > this->maxFd)
            this->maxFd = fd;
    }
    this->size_max = this->fds.size();
}

void Select::removeFD(int fdToRemove) {
    for (std::vector<int>::iterator it = fds.begin(); it < fds.end(); ++it) {
        if (*it == fdToRemove)
            fds.erase(it);
        break;
    }
}

int Select::launchSelect(std::string& buffer) {
    char buffer_c[4096];

    bzero(buffer_c, 4096);
    for (int fd : fds) {
        FD_SET(fd, &this->setRead);
        FD_SET(fd, &this->setWrite);
    }
    if (select(this->maxFd + 1, &this->setRead, &this->setWrite, NULL, NULL) == -1)
        throw IOError("Select Error");
    for (int i = 0; i < this->size_max; ++i) {
        if (FD_ISSET(fds[i], &this->setRead)) {
            read(fds[i], buffer_c, 4095);
            buffer.append(buffer_c);
            return i;
        }
    }
    return -1;
}
