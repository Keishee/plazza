/* 
 * File:   ParsePizza.cpp
 * Author: dana_a
 * 
 * Created on April 22, 2015, 4:19 PM
 */

#include <sstream>
#include <unistd.h>
#include <iostream>
#include <map>
#include <string>
#include "utils/Conversion.hpp"
#include "ParsePizza.hpp"

ParsePizza::ParsePizza() {
}

void ParsePizza::getBuffer(std::string str) {
    this->buffer = str;
}

bool ParsePizza::isMade() {
    int found;

    found = this->buffer.find("made:");
    if (found > 0) {
        if (this->buffer.at(found + 5) == '0')
            return (false);
        else if (this->buffer.at(found + 5) == '1')
            return (true);
    }
    return (false);
}

bool ParsePizza::isPacked() {
    int found;

    found = this->buffer.find("packed:");
    if (found > 0) {
        if (this->buffer.at(found + 7) == '0')
            return (false);
        else if (this->buffer.at(found + 7) == '1')
            return (true);
    }
    return (false);
}

TaillePizza ParsePizza::getSize() {
    int found;
    int size = 0;
    std::string nbr;
    std::map<int, TaillePizza> mapSize;

    mapSize[1] = TaillePizza::S;
    mapSize[2] = TaillePizza::M;
    mapSize[4] = TaillePizza::L;
    mapSize[8] = TaillePizza::XL;
    mapSize[16] = TaillePizza::XXL;

    found = this->buffer.find("size:");
    if (found >= 0) {
        nbr = this->buffer.substr(found + 5,
                this->buffer.find(SIZEDELIM) - (found + 5));
        size = Conversion<int>::toInt(nbr);
        return (mapSize[size]);
    }
    return (mapSize[size]);
}

TypePizza ParsePizza::getType() {
    int found;
    int type = 0;
    std::string nbr;
    std::map<int, TypePizza> mapType;

    mapType[1] = TypePizza::Regina;
    mapType[2] = TypePizza::Margarita;
    mapType[4] = TypePizza::Americaine;
    mapType[8] = TypePizza::Fantasia;

    found = this->buffer.find("type:");
    if (found >= 0) {
        nbr = this->buffer.substr(found + 5,
                this->buffer.find(TYPEDELIM) - (found + 5));
        type = Conversion<int>::toInt(nbr);
        return (mapType[type]);
    }
    return (mapType[type]);
}

Pizza *ParsePizza::getPizza(const std::string str) {
    this->getBuffer(str);

    Pizza *pizza = new Pizza(this->getSize(), this->getType());

    pizza->setMade(this->isMade());
    pizza->setPacked(this->isPacked());
    return (pizza);
}

ParsePizza::~ParsePizza() {
}

