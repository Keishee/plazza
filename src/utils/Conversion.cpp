/* 
 * File:   Conversion.cpp
 * Author: Clement Da Silva
 * 
 * Created on April 21, 2015, 12:42 PM
 */

#include "Conversion.hpp"


// OPERATORS ===================================================================

// GETTERS & SETTERS ===========================================================

// FUNCTIONS ===================================================================

template <typename T>
double Conversion<T>::toDouble(const std::string& nbr) {
    double _nbr;
    std::stringstream ss(nbr);
    ss >> _nbr;
    return _nbr;
}

template <typename T>
float Conversion<T>::toFloat(const std::string& nbr) {
    float _nbr;
    std::stringstream ss(nbr);
    ss >> _nbr;
    return _nbr;
}

template <typename T>
int Conversion<T>::toInt(const std::string& nbr) {
    int _nbr;
    std::stringstream ss(nbr);
    ss >> _nbr;
    return _nbr;
}

template<typename T>
double Conversion<T>::toDouble(const char* nbr) {
    double _nbr;
    std::stringstream ss(nbr);
    ss >> _nbr;
    return _nbr;
}

template<typename T>
float Conversion<T>::toFloat(const char* nbr) {
    double _nbr;
    std::stringstream ss(nbr);
    ss >> _nbr;
    return _nbr;
}

template<typename T>
int Conversion<T>::toInt(const char* nbr) {
    double _nbr;
    std::stringstream ss(nbr);
    ss >> _nbr;
    return _nbr;
}

template <typename T>
std::string Conversion<T>::toString(const T& nbr) {
    std::stringstream ss;
    ss << nbr;
    return ss.str();
}

template float Conversion<float>::toFloat(const std::string& nbr);
template double Conversion<double>::toDouble(const std::string& nbr);
template int Conversion<int>::toInt(const std::string& nbr);
template float Conversion<float>::toFloat(const char* nbr);
template double Conversion<double>::toDouble(const char* nbr);
template int Conversion<int>::toInt(const char* nbr);
template std::string Conversion<int>::toString(const int& nbr);
template std::string Conversion<float>::toString(const float& nbr);
template std::string Conversion<double>::toString(const double& nbr);
