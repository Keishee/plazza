/* 
 * File:   Errors.cpp
 * Author: Clement Da Silva
 * 
 * Created on March 31, 2015, 3:47 PM
 */

#include "Errors.hpp"
// DLERROR =====================================================================

DLError::DLError(const std::string& what) throw(){
    _what = "DLError: ";
    _what += what;
}

DLError& DLError::operator=(const DLError& right) {
    this->_what = right._what;
    return *this;
}

const char* DLError::what() const throw(){
    return this->_what.c_str();
}

DLError::~DLError() throw(){
}


// GRAPHICERROR ================================================================

GraphicError::GraphicError(const std::string& what) throw(){
    _what = "GraphicError: ";
    _what += what;
}

GraphicError& GraphicError::operator=(const GraphicError& right) {
    this->_what = right._what;
    return *this;
}

const char* GraphicError::what() const throw(){
    return this->_what.c_str();
}

GraphicError::~GraphicError() throw(){
}


// PARSEERROR ==================================================================

ParseError::~ParseError() throw(){
}

ParseError::ParseError(const std::string& what) throw(){
    this->_what = "ParseError: ";
    this->_what += what;
}

ParseError& ParseError::operator=(const ParseError& right) {
    this->_what = right._what;
    return *this;
}

const char* ParseError::what() const throw(){
    return this->_what.c_str();
}

// STANDARDERROR ===============================================================

StandardError::~StandardError() throw(){
}

StandardError::StandardError(const std::string& what) throw(){
    this->_what = "StandardError: ";
    this->_what += what;
}

StandardError& StandardError::operator=(const StandardError& right) {
    this->_what = right._what;
    return *this;
}

const char* StandardError::what() const throw(){
    return this->_what.c_str();
}

// IOERROR =====================================================================

IOError::~IOError() throw(){
}

IOError::IOError(const std::string& what) throw(){
    this->_what = "IOError: ";
    this->_what += what;
}

IOError& IOError::operator=(const IOError& right) {
    this->_what = right._what;
    return *this;
}

const char* IOError::what() const throw(){
    return this->_what.c_str();
}

// IOERROR =====================================================================

PTMCError::~PTMCError() throw(){
}

PTMCError::PTMCError(const std::string& what) throw(){
    this->_what = "PTMCError: ";
    this->_what += what;
}

PTMCError& PTMCError::operator=(const PTMCError& right) {
    this->_what = right._what;
    return *this;
}

const char* PTMCError::what() const throw(){
    return this->_what.c_str();
}
