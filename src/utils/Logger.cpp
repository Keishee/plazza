/* 
 * File:   Logger.cpp
 * Author: Clement Da Silva
 * 
 * Created on April 25, 2015, 3:56 PM
 */

#include <unistd.h>
#include <ctime>
#include <sstream>
#include <fstream>
#include "Logger.hpp"

Logger::Logger() {
}

Logger::~Logger() {
}

// OPERATORS ===================================================================

Logger& Logger::operator<<(const int& str) {
    std::ofstream file;
    file.open("log", std::ofstream::app);
    file << str;
    file.close();
    return *this;
}

Logger& Logger::operator<<(const unsigned int& str) {
    std::ofstream file;
    file.open("log", std::ofstream::app);
    file << str;
    file.close();
    return *this;
}

Logger& Logger::operator<<(const float& str) {
    std::ofstream file;
    file.open("log", std::ofstream::app);
    file << str;
    file.close();
    return *this;
}

Logger& Logger::operator<<(const double& str) {
    std::ofstream file;
    file.open("log", std::ofstream::app);
    file << str;
    file.close();
    return *this;
}

Logger& Logger::operator<<(const std::string& str) {
    std::ofstream file;
    file.open("log", std::ofstream::app);
    file << str;
    file.close();
    return *this;
}

// GETTERS & SETTERS ===========================================================

// FUNCTIONS ===================================================================

Logger* Logger::getInstance() {
    static Logger* logger;
    if (logger == NULL)
        logger = new Logger();
    return logger;
}

void Logger::log(const std::string& str) {
    std::ofstream file;
    file.open("log", std::ofstream::app);
    file << str;
    file.close();
}
