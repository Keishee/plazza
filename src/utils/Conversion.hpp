/* 
 * File:   Conversion.hpp
 * Author: Clement Da Silva
 *
 * Created on April 21, 2015, 12:42 PM
 */

#ifndef CONVERSION_HPP_
#define CONVERSION_HPP_

#include <iostream>
#include <sstream>

template <typename T>
class Conversion {
public:
    static std::string toString(const T& nbr);

    static int toInt(const std::string& nbr);
    static float toFloat(const std::string& nbr);
    static double toDouble(const std::string& nbr);

    static int toInt(const char* nbr);
    static float toFloat(const char* nbr);
    static double toDouble(const char* nbr);
};

#endif	/* !CONVERSION_HPP_ */

