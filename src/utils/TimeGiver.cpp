/* 
 * File:   TimeGiver.cpp
 * Author: Clement Da Silva
 * 
 * Created on March 29, 2015, 4:31 PM
 */

#include "TimeGiver.hpp"

/**
 * returns the nbr of µs since the epoch
 * @return 
 */
unsigned long long TimeGiver::timeMiS() {
    struct timeval tv;

    gettimeofday(&tv, NULL);
    tv.tv_sec *= 1000000;
    tv.tv_sec += tv.tv_usec;
    return static_cast<unsigned long long> (tv.tv_sec);
}
