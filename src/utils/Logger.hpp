/* 
 * File:   Logger.hpp
 * Author: Clement Da Silva
 *
 * Created on April 25, 2015, 3:56 PM
 */

#ifndef LOGGER_HPP_
#define LOGGER_HPP_

#include <iostream>
#include <fstream>

class Logger {
private:
    std::string date;
public:
    Logger();
    virtual ~Logger();

    void log(const std::string&);
    static Logger* getInstance();

    Logger& operator<<(const int&);
    Logger& operator<<(const unsigned&);
    Logger& operator<<(const double&);
    Logger& operator<<(const float&);
    Logger& operator<<(const std::string&);
};


#endif	/* !LOGGER_HPP_ */

