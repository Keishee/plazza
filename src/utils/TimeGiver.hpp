/* 
 * File:   TimeGiver.hpp
 * Author: Clement Da Silva
 *
 * Created on March 29, 2015, 4:31 PM
 */

#ifndef TIMEGIVER_HPP_
#define TIMEGIVER_HPP_

#include <sys/time.h>
#include <cstdlib>

class TimeGiver {
public:
    static unsigned long long timeMiS();
};

#endif	/* !TIMEGIVER_HPP_ */

