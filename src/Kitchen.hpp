/* 
 * File:   Kitchen.hpp
 * Author: dana_a
 *
 * Created on April 21, 2015, 4:30 PM
 */

#ifndef KITCHEN_HPP
#define	KITCHEN_HPP

#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "process/Processable.hpp"

class Kitchen : public Processable {
    int pipin;
    int pipout;
    int cooks;
    int kitchen;
    int regeneration;
    
public:
    Kitchen(const std::string& pipe_in, const std::string& pipe_out,
            const int cooks, const int kitchen,
            const int regeneration);
    virtual ~Kitchen();

    int action();
};

#endif	/* KITCHEN_HPP */

