/* 
 * File:   Pizza.cpp
 * Author: Clement Da Silva
 * 
 * Created on April 21, 2015, 5:02 PM
 */

#include <string>
#include <unistd.h>
#include "Pizza.hpp"

Mutex mutex;
std::vector<Condvar *> condVars;

Pizza::Pizza(TaillePizza size, TypePizza type) {
    this->_size = size;
    this->_type = type;
    this->_made = false;
    this->_packed = false;

    pizzaTypeMap = {
        {Regina, "regina"},
        {Margarita, "margarita"},
        {Americaine, "americaine"},
        {Fantasia, "fantasia"}
    };
    pizzaTailleMap = {
        {S, "S"},
        {M, "M"},
        {L, "L"},
        {XL, "XL"},
        {XXL, "XXL"}
    };

}

Pizza::Pizza(const Pizza& orig) {
    this->_size = orig._size;
    this->_type = orig._type;
    this->_made = orig._made;
    this->_packed = orig._packed;
}

Pizza::~Pizza() {
}

// OPERATORS ===================================================================

Pizza& Pizza::operator=(const Pizza& right) {
    if (this == &right)
        return *this;
    this->_size = right._size;
    this->_type = right._type;
    this->_made = right._made;
    this->_packed = right._packed;
    return *this;
}

// GETTERS & SETTERS ===========================================================

std::string Pizza::getRealSize() {
    std::string toReturn;
    
    toReturn.append("\x1b[32m")
            .append(pizzaTailleMap[_size])
            .append("\x1b[00m");
    return toReturn;
}

std::string Pizza::getRealType(){
    std::string toReturn;
    
    toReturn.append("\x1b[32m")
            .append(pizzaTypeMap[_type])
            .append("\x1b[00m");
    return toReturn;
}


TaillePizza Pizza::getTaillePizza() const {
    return this->_size;
}

TypePizza Pizza::getTypePizza() const {
    return this->_type;
}

bool Pizza::isMade() const {
    return this->_made;
}

bool Pizza::isPacked() const {
    return this->_packed;
}

void Pizza::setPacked(bool packed) {
    this->_packed = packed;
}

const char* Pizza::pack() {
    std::stringstream pack;
    this->_packed = true;
    pack << "size:"
            << _size
            << ";type:"
            << _type
            << "$made:"
            << _made
            << "£packed:"
            << _packed;
    return pack.str().c_str();
}

void Pizza::setMade(bool made) {
    this->_made = made;
}

void Pizza::setTaillePizza(TaillePizza size) {
    this->_size = size;
}

void Pizza::setTypePizza(TypePizza type) {
    this->_type = type;
}

void Pizza::unpack() {
    this->_packed = false;
}

// FUNCTIONS ===================================================================
