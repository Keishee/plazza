/* 
 * File:   imutex.hpp
 * Author: barbis_j
 *
 * Created on April 21, 2015, 4:27 PM
 */

#ifndef IMUTEX_HPP
#define	IMUTEX_HPP

#include <pthread.h>

class IMutex {
public:
    virtual ~IMutex(void);
    virtual void lock(void) = 0;
    virtual void unlock(void) = 0;
    virtual bool trylock(void) = 0;
    virtual pthread_mutex_t& getMutex(void) = 0;
};

#endif	/* IMUTEX_HPP */

