/* 
 * File:   mutex.hpp
 * Author: barbis_j
 *
 * Created on April 21, 2015, 4:22 PM
 */

#ifndef MUTEX_HPP
#define	MUTEX_HPP

#include <stdexcept>
#include "IMutex.hpp"
#include "../utils/Errors.hpp"

class Mutex : public IMutex {
public:
    Mutex();
    Mutex(const Mutex& orig);
    virtual ~Mutex();
    Mutex& operator=(const Mutex& orig);
    
    virtual void lock();
    virtual bool trylock();
    virtual void unlock();

    virtual pthread_mutex_t& getMutex();

private:
    pthread_mutex_t mutex;
};

#endif	/* MUTEX_HPP */

