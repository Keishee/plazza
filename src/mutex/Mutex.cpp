/* 
 * File:   mutex.cpp
 * Author: barbis_j
 * 
 * Created on April 21, 2015, 4:22 PM
 */

#include "Mutex.hpp"

Mutex::Mutex() {
    mutex = PTHREAD_MUTEX_INITIALIZER;
}

Mutex::Mutex(const Mutex& orig) {
    mutex = orig.mutex;
}

Mutex::~Mutex() {
}

IMutex::~IMutex(){
}

Mutex& Mutex::operator=(const Mutex& orig) {
    if (this == &orig)
        return *this;
    return *this;
}

void Mutex::lock() {
    if (pthread_mutex_lock(&mutex) != 0) {
        throw PTMCError("MutexLock Error");
    }
}

bool Mutex::trylock() {
    int ret = pthread_mutex_trylock(&mutex);
    if (ret != 0 && ret != EBUSY)
        throw PTMCError("MutexTrylock Error");
    return ret == EBUSY ? false : true;
}

void Mutex::unlock() {
    if (pthread_mutex_unlock(&mutex) != 0){
        throw PTMCError("MutexUnlock Error");
    }
}

pthread_mutex_t& Mutex::getMutex() {
    return mutex;
}
