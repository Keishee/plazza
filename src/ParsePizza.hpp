/* 
 * File:   ParsePizza.hpp
 * Author: dana_a
 *
 * Created on April 22, 2015, 4:19 PM
 */

#ifndef PARSEPIZZA_HPP
#define	PARSEPIZZA_HPP

#define SIZEDELIM   ';'
#define TYPEDELIM   '$'

#include "Pizza.hpp"


class ParsePizza {
    std::string buffer;
    
public:
    ParsePizza();
    virtual ~ParsePizza();

    void getBuffer(std::string str);

    bool isPacked();
    bool isMade();
    TaillePizza getSize();
    TypePizza getType();
    Pizza *getPizza(std::string str);
};

#endif	/* PARSEPIZZA_HPP */