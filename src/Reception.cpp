/* 
 * File:   Reception.cpp
 * Author: Clement Da Silva
 * 
 * Created on April 20, 2015, 8:32 PM
 */

#include "Reception.hpp"
#include "Select.hpp"
#include "UI/SDL.h"
#include "ParsePizza.hpp"
#include "utils/Logger.hpp"

float g_multiplier;

Reception::Reception(char* multiplier, char* cooks, char* ingTime) {
    if (!multiplier || !cooks || !ingTime)
        throw ParseError("Usage: ./plazza [mutliplier] [cooks/kitchen] [refuel time]");
    _multiplier = Conversion<float>::toFloat(multiplier);
    _cooks = Conversion<int>::toInt(cooks);
    _ingTime = Conversion<int>::toInt(ingTime);

    _kitchenCount = 0;
    typePizzaMap = {
        {"regina", Regina},
        {"margarita", Margarita},
        {"americaine", Americaine},
        {"fantasia", Fantasia}
    };
    taillePizzaMap = {
        {"S", S},
        {"M", M},
        {"L", L},
        {"XL", XL},
        {"XXL", XXL}
    };

    if (_multiplier <= 0 || _cooks <= 0 || _ingTime < 0 || _cooks > 25)
        throw ParseError("invalid arguments");
    g_multiplier = _multiplier;
}

Reception::Reception(const Reception& orig) {
    (void) orig;
}

Reception::~Reception() {
}

// FUNCTIONS ===================================================================

std::vector<Pizza *>* Reception::registerOrder(const std::string& order) {
    std::string tmp;
    TaillePizza pizzaSize;
    TypePizza pizzaType;
    std::istringstream iss(order);
    std::vector<Pizza *> *pizzas = new std::vector<Pizza *>;
    int nbr = 0;

    iss >> tmp;
    std::transform(tmp.begin(), tmp.end(), tmp.begin(), ::tolower);
    pizzaType = typePizzaMap[tmp];
    iss >> tmp;
    pizzaSize = taillePizzaMap[tmp];
    tmp.clear();
    iss >> tmp;
    if (!tmp.empty())
        nbr = Conversion<int>::toInt(tmp.substr(1, tmp.size()));
    if (nbr == 0)
        nbr = 1;
    if (pizzaSize == 0 || pizzaType == 0 || nbr < 0 || nbr > 99)
        throw ParseError("Pizza type/size/nbr doesn't exist");
    for (int i = 0; i < nbr; ++i)
        pizzas->push_back(new Pizza(pizzaSize, pizzaType));
    std::cout << "ordering " << nbr << " "
            << pizzas->at(0)->getRealType() << " of size "
            << pizzas->at(0)->getRealSize() << std::endl;
    (*Logger::getInstance()) << "ordering " << nbr << " "
            << pizzas->at(0)->getRealType() << " of size "
            << pizzas->at(0)->getRealSize() << "\n";

    return pizzas;
}

int Reception::createKitchen() {
    std::cout << "Creating kitchen " << _kitchenCount << std::endl;
    (*Logger::getInstance()) << "Creating kitchen " << _kitchenCount << "\n";
    std::string fifoInName("fifoIn");
    std::string fifoOutName("fifoOut");
    Kitchen* kitchen;

    fifoInName.append(Conversion<int>::toString(this->_kitchenCount));
    fifoOutName.append(Conversion<int>::toString(this->_kitchenCount));
    mkfifo(fifoInName.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
    mkfifo(fifoOutName.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);

    int fdin = open(fifoInName.c_str(), O_NONBLOCK | O_RDWR);
    int fdout = open(fifoOutName.c_str(), O_NONBLOCK | O_RDWR);
    if (fdin < 0 || fdout < 0)
        throw IOError("Couldn't create/open fifos");
    fifoInVector.push_back(fdin);
    fifoOutVector.push_back(fdout);
    select->addFd(fdin);
    fdKitchen[fdout] = _kitchenCount;
    kitchen = new Kitchen(fifoOutName,
            fifoInName,
            this->_cooks,
            this->_kitchenCount,
            this->_ingTime);
    Process *process = new Process(kitchen);
    process->launch();
    ++_kitchenCount;
    return 1;
}

void Reception::sendOrder(Pizza* pizza) {
    char buffer[4096];
    for (int i = 0; i < _kitchenCount; ++i) {
        bzero(buffer, 4096);
        write(fifoOutVector[i], "FULL", 4);
        usleep(500);
        read(fifoInVector[i], buffer, 4095);
        if (!strncmp(buffer, "GOOD", 4)) {
            write(fifoOutVector[i], pizza->pack(), strlen(pizza->pack()));
            std::cout << pizza->getRealType() << " sent to kitchen "
                    << fdKitchen[fifoOutVector[i]] << std::endl;
            (*Logger::getInstance()) << pizza->getRealType() << " sent to kitchen "
                    << fdKitchen[fifoOutVector[i]] << "\n";
            return;
        }
    }
    std::cout << "Kitchens are busy" << std::endl;
    (*Logger::getInstance()) << "Kitchens are busy" << "\n";
    createKitchen();
    write(fifoOutVector[_kitchenCount - 1], pizza->pack(), strlen(pizza->pack()) + 1);
    std::cout << pizza->getRealType() << " sent to kitchen "
            << fdKitchen[fifoOutVector[_kitchenCount - 1]] << std::endl;
    (*Logger::getInstance()) << pizza->getRealType() << " sent to kitchen "
            << fdKitchen[fifoOutVector[_kitchenCount - 1]] << "\n";

}

void Reception::getAndSendPizza(const std::string& serialPizza) {
    ParsePizza parse;
    Pizza* pizza;

    pizza = parse.getPizza(serialPizza);
    std::cout << "Your " << pizza->getRealType() << " "
            << pizza->getRealSize() << " is ready" << std::endl;
    (*Logger::getInstance()) << "Your " << pizza->getRealType() << " "
            << pizza->getRealSize() << " is ready\n";
    delete pizza;
}

void Reception::parseResult(const std::string& tmp) {
    if (!tmp.compare(0, 2, "-1")) {
        std::istringstream iss(tmp);
        int fdin, fdout, nbr;
        iss >> nbr;
        iss >> nbr;
        iss >> fdin;
        iss >> fdout;
        std::cout << "Kitchen " << nbr << " is closing" << std::endl;
        (*Logger::getInstance()) << "Kitchen " << nbr << " is closing\n";
        select->removeFD(fdin);
        close(fdin);
        close(fdout);
        std::string fifoName("fifoIn");
        fifoName.append(Conversion<int>::toString(nbr));
        unlink(fifoName.c_str());
        fifoName.clear();
        fifoName.append("fifoOut");
        fifoName.append(Conversion<int>::toString(nbr));
        unlink(fifoName.c_str());
    } else {
        getAndSendPizza(tmp);
    }
}

void Reception::begin() {
    std::string tmp;
    totalFDs.push_back(0);
    select = new Select(totalFDs);
    int fdReturn;

    while (1) {
        tmp.clear();
        if ((fdReturn = select->launchSelect(tmp)) == 0) {
            try {
                std::vector<Pizza *>* pizzas;
                pizzas = registerOrder(tmp);
                while (!pizzas->empty()) {
                    sendOrder(pizzas->back());
                    pizzas->pop_back();
                    usleep(10000);
                }
                delete pizzas;
            } catch (std::exception& e) {
                std::cerr << e.what() << std::endl;
            }
        } else if (fdReturn == -1) {
            ;
        } else {
            parseResult(tmp);
        }
        usleep(50);
    }
}
