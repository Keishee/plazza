/* 
 * File:   Cook.hpp
 * Author: barbis_j
 *
 * Created on April 22, 2015, 4:28 PM
 */

#ifndef COOK_HPP
#define	COOK_HPP

#include <iostream>
#include <map>
#include <unistd.h>
#include "Pizza.hpp"
#include "utils/Logger.hpp"
#include "process/PThread.hpp"
#include "UI/SDL.h"

class Cook {
public:
    Cook(unsigned int _id, std::vector<Pizza *> *_cookedPizzas, Mutex *_lockPizza, int _kitchenId, SDL *sdl);
    Cook(const Cook& orig);
    virtual ~Cook();
    Cook& operator=(const Cook& orig);
    void cookPizza(Pizza *pizza);
    
    std::map<TypePizza, int> getCookingTime() const;
    void setCookingTime(std::map<TypePizza, int> cookingTime);
    Pizza *getCurrentPizza();
    bool getIsCooking() const;
    void setIsCooking(bool Cooking);
    PThread* getThread() const;
    void setThread(PThread* thread);
    unsigned int getId() const;
    std::vector<Pizza *> *getCookedPizza();
    Mutex *getLockPizza();
    int getKitchenId() const;
    SDL *getSdl();

private:
    bool isCooking;
    std::map<TypePizza, int> cookingTime;
    PThread *thread;
    Pizza *currentPizza;
    unsigned int id;
    std::vector<Pizza *> *cookedPizzas;
    Mutex *lockPizza;
    int kitchenId;
    SDL *_sdl;
};

void *tmp(void *arg);

#endif	/* COOK_HPP */

