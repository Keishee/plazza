/* 
 * File:   SDL.cpp
 * Author: dana_a
 * 
 * Created on April 23, 2015, 3:24 PM
 */

#include <SDL/SDL.h>
#include "../utils/Errors.hpp"
#include <iostream>
#include <vector>
#include "SDL.h"

SDL::SDL(const int cooks, const int kitchen) {
    if ((SDL_Init(SDL_INIT_VIDEO)) < 0)
        throw GraphicError("Couldn't init SDL");
    this->window = SDL_SetVideoMode(SIZECOOK * 10, SIZECOOK, 8, SDL_SWSURFACE | SDL_DOUBLEBUF);
    this->id_kitchen = kitchen;
    for (int i = 0; i < cooks; ++i) {
        this->cooks.push_back(false);
    }
    this->cooksOff = SDL_LoadBMP("cuisinier_nowork.bmp");
    this->cooksOn = SDL_LoadBMP("cuisinier_cooked.bmp");
}

SDL::~SDL() {
    SDL_Quit();
}

void SDL::drawCooks() {
    SDL_Rect pos;

    pos.x = 0;
    pos.y = 0;
    for (unsigned int i = 0; i < this->cooks.size(); ++i) {
        if (this->cooks[i] == false) {
            SDL_BlitSurface(cooksOff, NULL, this->window, &pos);
        } else
            SDL_BlitSurface(cooksOn, NULL, this->window, &pos);
        pos.x += SIZECOOK;
        SDL_Flip(this->window);
    }
}

void SDL::setState(int i, bool states) {
    this->cooks[i] = states;
}
