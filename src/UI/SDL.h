/* 
 * File:   SDL.h
 * Author: dana_a
 *
 * Created on April 23, 2015, 3:24 PM
 */

#ifndef SDL_H
#define	SDL_H
#include <SDL/SDL.h>

#define SIZECOOK   47

class SDL {
    SDL_Surface *window;
    SDL_Surface *cooksOn;
    SDL_Surface *cooksOff;
    std::vector<bool> cooks;
    int id_kitchen;

public:
    SDL(const int cooks, const int kitchen);
    virtual ~SDL();
    void drawCooks();
    void setState(int i, bool states);
};

#endif	/* SDL_H */

