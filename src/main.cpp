/* 
 * File:   testmain.cpp
 * Author: Clement Da Silva
 *
 * Created on April 20, 2015, 8:28 PM
 */

#include <cstdlib>
#include <iostream>
#include "Reception.hpp"

#include <fstream>
int main(int ac, char **av) {
    (void) ac;
    try {
        Reception rec(av[1], av[2], av[3]);
        rec.begin();
    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
    }
    return 0;
}

